# Powershell

This gitlab project and repository was created to give me an opportunity to practice using gitlab and uploading my powershell code to a repository.

I hope to organize my powershell code into subjects and provide a easy to pull from toolbox with production ready code.

Code will be developed in branches from the dev branch.

Once code is ready to be pushed to main, you will complete the following steps.

1. Open a pull request for your branch to be merged into dev.
2. Obtain approval for your pull request.
3. Merge your code into dev.
4. Open a pull request with main.
5. Obtain approval for your pull request.
6. Merge your code into main.
